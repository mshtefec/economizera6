import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  displayedColumns = ['id', 'mark', 'name', 'price'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}


export interface PeriodicElement {
  id: number;
  mark: string;
  name: string;
  price: number;
  //symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {id: 1, mark: 'carrefour', name: 'luchetti', price: 22.6},
  {id: 2, mark: 'carrefour', name: 'luchetti', price: 22.6},
  {id: 3, mark: 'carrefour', name: 'luchetti', price: 22.6},
  {id: 4, mark: 'carrefour', name: 'luchetti', price: 22.6},
  {id: 5, mark: 'carrefour', name: 'luchetti', price: 22.6},
  {id: 6, mark: 'carrefour', name: 'luchetti', price: 22.6},
  {id: 7, mark: 'carrefour', name: 'luchetti', price: 22.6},
  {id: 8, mark: 'carrefour', name: 'luchetti', price: 22.6},
];
