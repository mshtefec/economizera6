import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommerceFormComponent } from './commerce-form.component';

describe('CommerceFormComponent', () => {
  let component: CommerceFormComponent;
  let fixture: ComponentFixture<CommerceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommerceFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommerceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
