import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommerceShowComponent } from './commerce-show.component';

describe('CommerceShowComponent', () => {
  let component: CommerceShowComponent;
  let fixture: ComponentFixture<CommerceShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommerceShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommerceShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
