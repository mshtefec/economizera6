import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BackendComponent } from './backend.component';
import { ProductListComponent } from './components/products/product-list/product-list.component';
import { ProductFormComponent } from './components/products/product-form/product-form.component';
import { ProductShowComponent } from './components/products/product-show/product-show.component';
import { CommerceListComponent } from './components/business/commerce-list/commerce-list.component';
import { CommerceFormComponent } from './components/business/commerce-form/commerce-form.component';
import { CommerceShowComponent } from './components/business/commerce-show/commerce-show.component';
import { Page404Component } from './components/page404/page404.component';

const routes: Routes = [
  { path: '', redirectTo: 'products', pathMatch: 'full' },
  { path: 'admin', component: BackendComponent,
    children: [
      { path: '', redirectTo: 'products', pathMatch: 'full' },
      { path: '',
        children: [
          { path: 'products', component: ProductListComponent },
          { path: 'products/new', component: ProductFormComponent },
          { path: 'products/{id}', component: ProductShowComponent },
          { path: 'business', component: CommerceListComponent },
          { path: 'business/new', component: CommerceFormComponent },
          { path: 'business/{id}', component: CommerceShowComponent },
        ]
      },
      { path: '**', component: Page404Component },
    ]
  },
  { path: '**', component: Page404Component }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackendRoutingModule { }
